import bassD;
import std.string;
import std.stdio;
import core.sys.windows.windows;
import core.thread;
import core.time;
import std.conv;

void main(string[] args) {
	DWORD chan,act,time,level;
	BOOL ismod;
	QWORD pos;
	int a;

	writeln("Simple console mode BASS example : MOD/MPx/OGG/WAV player\n"
			"---------------------------------------------------------\n");

	// check the correct BASS was loaded
	//if (HIWORD(BASS_GetVersion()) != BASSVERSION) {
	//	writeln("An incorrect version of BASS was loaded");
	//	return;
	//}
	
	if (args.length != 2) {
		writeln("\tusage: contest <file>\n");
		return;
	}
	
	// setup output - default device
	if (!BASS_Init(-1, 44100, 0, null, null))
		Error("Can't initialize device");
		
	// try streaming the file/url
	chan = BASS_StreamCreateFile(0, wtext(args[1]), 0, 0, BASS_SAMPLE_LOOP | BASS_UNICODE);
	if (!chan) {
		chan = BASS_StreamCreateURL(wtext(args[1]), 0, BASS_SAMPLE_LOOP | BASS_UNICODE, null, null);
	} 
	if (chan) {
		pos = BASS_ChannelGetLength(chan, BASS_POS_BYTE);
		if (BASS_StreamGetFilePosition(chan,BASS_FILEPOS_DOWNLOAD) != -1) {
			// streaming from the internet
			if (pos!=-1)
				writeln("streaming internet file [", pos, " bytes]");
			else
				writeln("streaming internet file");
		}
		else
			writeln("streaming file [", pos, " bytes]");
		
		ismod = FALSE;
	} else {
		// try loading the MOD (with looping, sensitive ramping, and calculate the duration)
		chan = BASS_MusicLoad(FALSE, wtext(args[1]),0,0,BASS_SAMPLE_LOOP|BASS_MUSIC_RAMPS|BASS_MUSIC_PRESCAN,1);
		if (!chan)
			// not a MOD either
			Error("Can't play the file");
		{ // count channels
			float dummy;
			for (a = 0; BASS_ChannelGetAttribute(chan, BASS_ATTRIB_MUSIC_VOL_CHAN + a, &dummy); a++){}
		}
		write(format("playing MOD music \"%s\" [%u chans, %u orders]",
			BASS_ChannelGetTags(chan, BASS_TAG_MUSIC_NAME), a, cast(DWORD)BASS_ChannelGetLength(chan, BASS_POS_MUSIC_ORDER)));
		pos = BASS_ChannelGetLength(chan, BASS_POS_BYTE);
		ismod = TRUE;
	}
	
	// display the time length
	if (pos != -1) {
		time = cast(DWORD)BASS_ChannelBytes2Seconds(chan, pos);
		writeln(format("%u:%u", time/60, time%60));
	} else // no time length available
		writeln("");

	BASS_ChannelPlay(chan, 0);
	
	act = BASS_ChannelIsActive(chan);
	while (act) {
		// display some stuff and wait a bit
		level = BASS_ChannelGetLevel(chan);
		pos = BASS_ChannelGetPosition(chan, BASS_POS_BYTE);
		time = cast(DWORD)BASS_ChannelBytes2Seconds(chan, pos);
		write("pos ", pos);
		write(format(" - %u:%u - L ", time/60, time%60));
		if (act == BASS_ACTIVE_STALLED) { // playback has stalled
			write(format("-- buffering : %u --", cast(DWORD)BASS_StreamGetFilePosition(chan, BASS_FILEPOS_BUFFER)));
		} else {
			for (a = 27204; a > 200; a = a*2/3) write(LOWORD(level) >= a ? '*' : '-');
			write(' ');
			for (a = 210; a < 32768; a = a*3/2) write(HIWORD(level) >= a ? '*' : '-');
		}
		write(format(" R - cpu %.2f  \r", BASS_GetCPU()));
		stdout.flush();
		Thread.sleep(dur!("msecs")( 50 ));
		act = BASS_ChannelIsActive(chan);
	}
	
	// wind the frequency down...
	BASS_ChannelSlideAttribute(chan, BASS_ATTRIB_FREQ, 1000, 500);
	Sleep(300);
	// ...and fade-out to avoid a "click"
	BASS_ChannelSlideAttribute(chan,BASS_ATTRIB_VOL,-1,200);
	// wait for slide to finish
	while (BASS_ChannelIsSliding(chan,0)) Sleep(1);

	BASS_Free();
}

void Error(string text) 
{
	writeln(format("Error(%d): %s\n", BASS_ErrorGetCode(), text));
	BASS_Free();
}