module bassD;

import std.conv;
import std.exception;
import std.stdio;
import std.string;

version (Windows) {
    import core.sys.windows.windows;

    alias LoadLibraryA loadLibrary;
    alias FreeLibrary unloadLibrary;
    alias GetProcAddress getProc;
    alias GetLastError lastError;
	
	string libPath = "bass.dll";
} else version (linux) { // Linux, Mac OS etc.
    import core.sys.linux.dlfcn;

    alias dlclose unloadLibrary;
    alias dlsym getProc;
    alias dlerror lastError;

    void* loadLibrary(const char* path, const int mode = RTLD_LAZY)
    {
        return dlopen(path, mode);
    }
	
	string libPath = "./libbass.so";
	
	ubyte LOBYTE(ushort a) { return cast(ubyte)a; } 
	ubyte HIBYTE(ushort a) { return cast(ubyte)((a)>>8); }
	ushort LOWORD(uint a) { return cast(ushort)(a); }
	ushort HIWORD(uint a) { return cast(ushort)((a)>>16); }
	ushort MAKEWORD(ushort a, ushort b) { return cast(ushort)(((a)&0xff)|((b)<<8)); }
	ulong MAKELONG(uint a, uint b) { return cast(ulong)(((a)&0xffff)|((b)<<16)); }
} else {
    static assert(false, "Unsupported OS version!");
}

private static bool initialized = false;

const int BASSVERSION = 0x204;
string BASSVERSIONTEXT = "2.4";

alias ulong QWORD;

alias uint HMUSIC; 	// MOD music handle
alias uint HSAMPLE; 	// sample handle
alias uint HCHANNEL; 	// playing sample's channel handle
alias uint HSTREAM; 	// sample stream handle
alias uint HRECORD; 	// recording handle
alias uint HSYNC; 		// synchronizer handle
alias uint HDSP; 		// DSP handle
alias uint HFX; 		// DX8 effect handle
alias uint HPLUGIN; 	// Plugin handle

extern (Windows) {
	const int BASS_OK = 0;
	const int BASS_ERROR_MEM = 1;
	const int BASS_ERROR_FILEOPEN = 2;
	const int BASS_ERROR_DRIVER = 3;
	const int BASS_ERROR_BUFLOST = 4;
	const int BASS_ERROR_HANDLE = 5;
	const int BASS_ERROR_FORMAT = 6;
	const int BASS_ERROR_POSITION = 7;
	const int BASS_ERROR_INIT = 8;
	const int BASS_ERROR_START = 9;
	const int BASS_ERROR_ALREADY = 14;
	const int BASS_ERROR_NOCHAN = 18;
	const int BASS_ERROR_ILLTYPE = 19;
	const int BASS_ERROR_ILLPARAM = 20;
	const int BASS_ERROR_NO3D = 21;
	const int BASS_ERROR_NOEAX = 22;
	const int BASS_ERROR_DEVICE = 23;
	const int BASS_ERROR_NOPLAY = 24;
	const int BASS_ERROR_FREQ = 25;
	const int BASS_ERROR_NOTFILE = 27;
	const int BASS_ERROR_NOHW = 29;
	const int BASS_ERROR_EMPTY = 31;
	const int BASS_ERROR_NONET = 32;
	const int BASS_ERROR_CREATE = 33;
	const int BASS_ERROR_NOFX = 34;
	const int BASS_ERROR_NOTAVAIL = 37;
	const int BASS_ERROR_DECODE = 38;
	const int BASS_ERROR_DX = 39;
	const int BASS_ERROR_TIMEOUT = 40;
	const int BASS_ERROR_FILEFORM = 41;
	const int BASS_ERROR_SPEAKER = 42;
	const int BASS_ERROR_VERSION = 43;
	const int BASS_ERROR_CODEC = 44;
	const int BASS_ERROR_ENDED = 45;
	const int BASS_ERROR_BUSY = 46;
	const int BASS_ERROR_UNKNOWN = -1;

	// BASS_SetConfig options
	const int BASS_CONFIG_BUFFER = 0;
	const int BASS_CONFIG_UPDATEPERIOD = 1;
	const int BASS_CONFIG_GVOL_SAMPLE = 4;
	const int BASS_CONFIG_GVOL_STREAM = 5;
	const int BASS_CONFIG_GVOL_MUSIC = 6;
	const int BASS_CONFIG_CURVE_VOL = 7;
	const int BASS_CONFIG_CURVE_PAN = 8;
	const int BASS_CONFIG_FLOATDSP = 9;
	const int BASS_CONFIG_3DALGORITHM = 10;
	const int BASS_CONFIG_NET_TIMEOUT = 11;
	const int BASS_CONFIG_NET_BUFFER = 12;
	const int BASS_CONFIG_PAUSE_NOPLAY = 13;
	const int BASS_CONFIG_NET_PREBUF = 15;
	const int BASS_CONFIG_NET_PASSIVE = 18;
	const int BASS_CONFIG_REC_BUFFER = 19;
	const int BASS_CONFIG_NET_PLAYLIST = 21;
	const int BASS_CONFIG_MUSIC_VIRTUAL = 22;
	const int BASS_CONFIG_VERIFY = 23;
	const int BASS_CONFIG_UPDATETHREADS = 24;
	const int BASS_CONFIG_DEV_BUFFER = 27;
	const int BASS_CONFIG_VISTA_TRUEPOS = 30;
	const int BASS_CONFIG_IOS_MIXAUDIO = 34;
	const int BASS_CONFIG_DEV_DEFAULT = 36;
	const int BASS_CONFIG_NET_READTIMEOUT = 37;
	const int BASS_CONFIG_VISTA_SPEAKERS = 38;
	const int BASS_CONFIG_IOS_SPEAKER = 39;
	const int BASS_CONFIG_HANDLES = 41;
	const int BASS_CONFIG_UNICODE = 42;
	const int BASS_CONFIG_SRC = 43;
	const int BASS_CONFIG_SRC_SAMPLE = 44;
	const int BASS_CONFIG_ASYNCFILE_BUFFER = 45;
	const int BASS_CONFIG_OGG_PRESCAN = 47;

	// BASS_SetConfigPtr options
	const int BASS_CONFIG_NET_AGENT = 16;
	const int BASS_CONFIG_NET_PROXY = 17;
	const int BASS_CONFIG_IOS_NOTIFY = 46;

	// BASS_Init flags
	const int BASS_DEVICE_8BITS = 1;// use 8 bit resolution, else 16 bit
	const int BASS_DEVICE_MONO = 2;// use mono, else stereo
	const int BASS_DEVICE_3D = 4;// enable 3D functionality
	const int BASS_DEVICE_LATENCY = 0x100;// calculate device latency (BASS_INFO struct)
	const int BASS_DEVICE_CPSPEAKERS = 0x400;// detect speakers via Windows control panel
	const int BASS_DEVICE_SPEAKERS = 0x800;// force enabling of speaker assignment
	const int BASS_DEVICE_NOSPEAKER = 0x1000;// ignore speaker arrangement
	const int BASS_DEVICE_DMIX = 0x2000;// use ALSA "dmix" plugin
	const int BASS_DEVICE_FREQ = 0x4000;// set device sample rate

	// DirectSound interfaces (for use with BASS_GetDSoundObject)
	const int BASS_OBJECT_DS = 1;// IDirectSound
	const int BASS_OBJECT_DS3DL = 2;// IDirectSound3DListener

	// Device info structure
	version (Windows) {
		struct BASS_DEVICEINFO {
			const wchar *name;	// description
			const wchar *driver;	// driver
			DWORD flags;
		}
	} else {
		struct BASS_DEVICEINFO {
			const char *name;	// description
			const char *driver;	// driver
			DWORD flags;
		}
	}

	// BASS_DEVICEINFO flags
	const int BASS_DEVICE_ENABLED = 1;
	const int BASS_DEVICE_DEFAULT = 2;
	const int BASS_DEVICE_INIT = 4;

	struct BASS_INFO {
		DWORD flags;	// device capabilities (const int DSCAPS_xxx flags)
		DWORD hwsize;	// size of total device hardware memory
		DWORD hwfree;	// size of free device hardware memory
		DWORD freesam;	// number of free sample slots in the hardware
		DWORD free3d;	// number of free 3D sample slots in the hardware
		DWORD minrate;	// min sample rate supported by the hardware
		DWORD maxrate;	// max sample rate supported by the hardware
		BOOL eax;		// device supports EAX? (always FALSE if BASS_DEVICE_3D was not used)
		DWORD minbuf;	// recommended minimum buffer length in ms (requires BASS_DEVICE_LATENCY)
		DWORD dsver;	// DirectSound version
		DWORD latency;	// delay (in ms) before start of playback (requires BASS_DEVICE_LATENCY)
		DWORD initflags; // BASS_Init "flags" parameter
		DWORD speakers; // number of speakers available
		DWORD freq;		// current output rate
	}

	// BASS_INFO flags (from DSOUND.H)
	const int DSCAPS_CONTINUOUSRATE = 0x00000010;// supports all sample rates between min/maxrate
	const int DSCAPS_EMULDRIVER = 0x00000020;// device does NOT have hardware DirectSound support
	const int DSCAPS_CERTIFIED = 0x00000040;// device driver has been certified by Microsoft
	const int DSCAPS_SECONDARYMONO = 0x00000100;// mono
	const int DSCAPS_SECONDARYSTEREO = 0x00000200;// stereo
	const int DSCAPS_SECONDARY8BIT = 0x00000400;// 8 bit
	const int DSCAPS_SECONDARY16BIT = 0x00000800;// 16 bit

	// Recording device info structure
	struct BASS_RECORDINFO {
		DWORD flags;	// device capabilities (DSCCAPS_xxx flags)
		DWORD formats;	// supported standard formats (const int WAVE_FORMAT_xxx flags)
		DWORD inputs;	// number of inputs
		BOOL singlein;	// TRUE = only 1 input can be set at a time
		DWORD freq;		// current input rate
	}

	// BASS_RECORDINFO flags (from DSOUND.H)
	const int DSCCAPS_EMULDRIVER = DSCAPS_EMULDRIVER;// device does NOT have hardware DirectSound recording support
	const int DSCCAPS_CERTIFIED = DSCAPS_CERTIFIED;// device driver has been certified by Microsoft

	// defines for formats field of BASS_RECORDINFO (from MMSYSTEM.H)
	const int WAVE_FORMAT_1M08 = 0x00000001;// 11.025 kHz, Mono,   8-bit
	const int WAVE_FORMAT_1S08 = 0x00000002;// 11.025 kHz, Stereo, 8-bit
	const int WAVE_FORMAT_1M16 = 0x00000004;// 11.025 kHz, Mono,   16-bit
	const int WAVE_FORMAT_1S16 = 0x00000008;// 11.025 kHz, Stereo, 16-bit
	const int WAVE_FORMAT_2M08 = 0x00000010;// 22.05  kHz, Mono,   8-bit
	const int WAVE_FORMAT_2S08 = 0x00000020;// 22.05  kHz, Stereo, 8-bit
	const int WAVE_FORMAT_2M16 = 0x00000040;// 22.05  kHz, Mono,   16-bit
	const int WAVE_FORMAT_2S16 = 0x00000080;// 22.05  kHz, Stereo, 16-bit
	const int WAVE_FORMAT_4M08 = 0x00000100;// 44.1   kHz, Mono,   8-bit
	const int WAVE_FORMAT_4S08 = 0x00000200;// 44.1   kHz, Stereo, 8-bit
	const int WAVE_FORMAT_4M16 = 0x00000400;// 44.1   kHz, Mono,   16-bit
	const int WAVE_FORMAT_4S16 = 0x00000800;// 44.1   kHz, Stereo, 16-bit

	// Sample info structure
	struct BASS_SAMPLE {
		DWORD freq;		// default playback rate
		float volume;	// default volume (0-1)
		float pan;		// default pan (-1=left, 0=middle, 1=right)
		DWORD flags;	// BASS_SAMPLE_xxx flags
		DWORD length;	// length (in bytes)
		DWORD max;		// maximum simultaneous playbacks
		DWORD origres;	// original resolution bits
		DWORD chans;	// number of channels
		DWORD mingap;	// minimum gap (ms) between creating channels
		DWORD mode3d;	// BASS_3DMODE_xxx mode
		float mindist;	// minimum distance
		float maxdist;	// maximum distance
		DWORD iangle;	// angle of inside projection cone
		DWORD oangle;	// angle of outside projection cone
		float outvol;	// delta-volume outside the projection cone
		DWORD vam;		// voice allocation/management flags (BASS_VAM_xxx)
		DWORD priority;	// priority (0=lowest, 0xffffffff=highest)
	}

	const int BASS_SAMPLE_8BITS = 1;// 8 bit
	const int BASS_SAMPLE_FLOAT = 256;// 32-bit floating-point
	const int BASS_SAMPLE_MONO = 2;// mono
	const int BASS_SAMPLE_LOOP = 4;// looped
	const int BASS_SAMPLE_3D = 8;// 3D functionality
	const int BASS_SAMPLE_SOFTWARE = 16;// not using hardware mixing
	const int BASS_SAMPLE_MUTEMAX = 32;// mute at max distance (3D only)
	const int BASS_SAMPLE_VAM = 64;// DX7 voice allocation & management
	const int BASS_SAMPLE_FX = 128;// old implementation of DX8 effects
	const int BASS_SAMPLE_OVER_VOL = 0x10000;// override lowest volume
	const int BASS_SAMPLE_OVER_POS = 0x20000;// override longest playing
	const int BASS_SAMPLE_OVER_DIST = 0x30000;// override furthest from listener (3D only)
	const int BASS_STREAM_PRESCAN = 0x20000;// enable pin-point seeking/length (MP3/MP2/MP1)
	const int BASS_MP3_SETPOS = BASS_STREAM_PRESCAN;
	const int BASS_STREAM_AUTOFREE = 0x40000;// automatically free the stream when it stop/ends
	const int BASS_STREAM_RESTRATE = 0x80000;// restrict the download rate of internet file streams
	const int BASS_STREAM_BLOCK = 0x100000;// download/play internet file stream in small blocks
	const int BASS_STREAM_DECODE = 0x200000;// don't play the stream, only decode (BASS_ChannelGetData)
	const int BASS_STREAM_STATUS = 0x800000;// give server status info (HTTP/ICY tags) in DOWNLOADPROC

	const int BASS_MUSIC_FLOAT = BASS_SAMPLE_FLOAT;
	const int BASS_MUSIC_MONO = BASS_SAMPLE_MONO;
	const int BASS_MUSIC_LOOP = BASS_SAMPLE_LOOP;
	const int BASS_MUSIC_3D = BASS_SAMPLE_3D;
	const int BASS_MUSIC_FX = BASS_SAMPLE_FX;
	const int BASS_MUSIC_AUTOFREE = BASS_STREAM_AUTOFREE;
	const int BASS_MUSIC_DECODE = BASS_STREAM_DECODE;
	const int BASS_MUSIC_PRESCAN = BASS_STREAM_PRESCAN;// calculate playback length
	const int BASS_MUSIC_CALCLEN = BASS_MUSIC_PRESCAN;
	const int BASS_MUSIC_RAMP = 0x200;// normal ramping
	const int BASS_MUSIC_RAMPS = 0x400;// sensitive ramping
	const int BASS_MUSIC_SURROUND = 0x800;// surround sound
	const int BASS_MUSIC_SURROUND2 = 0x1000;// surround sound (mode 2)
	const int BASS_MUSIC_FT2MOD = 0x2000;// play .MOD as FastTracker 2 does
	const int BASS_MUSIC_PT1MOD = 0x4000;// play .MOD as ProTracker 1 does
	const int BASS_MUSIC_NONINTER = 0x10000;// non-interpolated sample mixing
	const int BASS_MUSIC_SINCINTER = 0x800000;// sinc interpolated sample mixing
	const int BASS_MUSIC_POSRESET = 0x8000;// stop all notes when moving position
	const int BASS_MUSIC_POSRESETEX = 0x400000;// stop all notes and reset bmp/etc when moving position
	const int BASS_MUSIC_STOPBACK = 0x80000;// stop the music on a backwards jump effect
	const int BASS_MUSIC_NOSAMPLE = 0x100000;// don't load the samples

	// Speaker assignment flags
	const int BASS_SPEAKER_FRONT = 0x1000000;// front speakers
	const int BASS_SPEAKER_REAR = 0x2000000;// rear/side speakers
	const int BASS_SPEAKER_CENLFE = 0x3000000;// center & LFE speakers (5.1)
	const int BASS_SPEAKER_REAR2 = 0x4000000;// rear center speakers (7.1)
	int BASS_SPEAKER_N(uint n) { return ((n)<<24); }// n'th pair of speakers (max 15)
	const int BASS_SPEAKER_LEFT = 0x10000000;// modifier: left
	const int BASS_SPEAKER_RIGHT = 0x20000000;// modifier: right
	const int BASS_SPEAKER_FRONTLEFT = BASS_SPEAKER_FRONT|BASS_SPEAKER_LEFT;
	const int BASS_SPEAKER_FRONTRIGHT = BASS_SPEAKER_FRONT|BASS_SPEAKER_RIGHT;
	const int BASS_SPEAKER_REARLEFT = BASS_SPEAKER_REAR|BASS_SPEAKER_LEFT;
	const int BASS_SPEAKER_REARRIGHT = BASS_SPEAKER_REAR|BASS_SPEAKER_RIGHT;
	const int BASS_SPEAKER_CENTER = BASS_SPEAKER_CENLFE|BASS_SPEAKER_LEFT;
	const int BASS_SPEAKER_LFE = BASS_SPEAKER_CENLFE|BASS_SPEAKER_RIGHT;
	const int BASS_SPEAKER_REAR2LEFT = BASS_SPEAKER_REAR2|BASS_SPEAKER_LEFT;
	const int BASS_SPEAKER_REAR2RIGHT = BASS_SPEAKER_REAR2|BASS_SPEAKER_RIGHT;
	const int BASS_ASYNCFILE = 0x40000000;
	const int BASS_UNICODE = 0x80000000;//-2147483648
	const int BASS_RECORD_PAUSE = 0x8000;// start recording paused

	// DX7 voice allocation & management flags
	const int BASS_VAM_HARDWARE = 1;
	const int BASS_VAM_SOFTWARE = 2;
	const int BASS_VAM_TERM_TIME = 4;
	const int BASS_VAM_TERM_DIST = 8;
	const int BASS_VAM_TERM_PRIO = 16;

	struct BASS_CHANNELINFO {
		DWORD freq;		// default playback rate
		DWORD chans;	// channels
		DWORD flags;	// BASS_SAMPLE/STREAM/MUSIC/SPEAKER flags
		DWORD ctype;	// type of channel
		DWORD origres;	// original resolution
		HPLUGIN plugin;	// plugin
		HSAMPLE sample; // sample
		char *filename; // filename
	}

	const int BASS_CTYPE_SAMPLE = 1;
	const int BASS_CTYPE_RECORD = 2;
	const int BASS_CTYPE_STREAM = 0x10000;
	const int BASS_CTYPE_STREAM_OGG = 0x10002;
	const int BASS_CTYPE_STREAM_MP1 = 0x10003;
	const int BASS_CTYPE_STREAM_MP2 = 0x10004;
	const int BASS_CTYPE_STREAM_MP3 = 0x10005;
	const int BASS_CTYPE_STREAM_AIFF = 0x10006;
	const int BASS_CTYPE_STREAM_CA = 0x10007;
	const int BASS_CTYPE_STREAM_MF = 0x10008;
	const int BASS_CTYPE_STREAM_WAV = 0x40000;// WAVE flag, LOWORD=codec
	const int BASS_CTYPE_STREAM_WAV_PCM = 0x50001;
	const int BASS_CTYPE_STREAM_WAV_FLOAT = 0x50003;
	const int BASS_CTYPE_MUSIC_MOD = 0x20000;
	const int BASS_CTYPE_MUSIC_MTM = 0x20001;
	const int BASS_CTYPE_MUSIC_S3M = 0x20002;
	const int BASS_CTYPE_MUSIC_XM = 0x20003;
	const int BASS_CTYPE_MUSIC_IT = 0x20004;
	const int BASS_CTYPE_MUSIC_MO3 = 0x00100;// MO3 flag

	struct BASS_PLUGINFORM {
		DWORD ctype;		// channel type
		version (Windows) {
			const wchar *name;	// format description
			const wchar *exts;	// file extension filter (*.ext1;*.ext2;etc...)
		} else {
			const char *name;	// format description
			const char *exts;	// file extension filter (*.ext1;*.ext2;etc...)
		}
	}

	struct BASS_PLUGININFO {
		DWORD dwversion;					// version (same form as BASS_GetVersion)
		DWORD formatc;					// number of formats
		const BASS_PLUGINFORM *formats;	// the array of formats
	}

	// 3D vector (for 3D positions/velocities/orientations)
	struct BASS_3DVECTOR {
		this(float _x, float _y, float _z){
			this.x = _x;
			this.y = _y;
			this.z = _z;
		}

		float x;	// +=right, -=left
		float y;	// +=up, -=down
		float z;	// +=front, -=behind
	}

	// 3D channel modes
	const int BASS_3DMODE_NORMAL = 0;
	const int BASS_3DMODE_RELATIVE = 1;
	const int BASS_3DMODE_OFF = 2;

	// software 3D mixing algorithms (used with BASS_CONFIG_3DALGORITHM)
	const int BASS_3DALG_DEFAULT = 0;
	const int BASS_3DALG_OFF = 1;
	const int BASS_3DALG_FULL = 2;
	const int BASS_3DALG_LIGHT = 3;

	// EAX environments, use with BASS_SetEAXParameters
	enum
	{
		EAX_ENVIRONMENT_GENERIC,
		EAX_ENVIRONMENT_PADDEDCELL,
		EAX_ENVIRONMENT_ROOM,
		EAX_ENVIRONMENT_BATHROOM,
		EAX_ENVIRONMENT_LIVINGROOM,
		EAX_ENVIRONMENT_STONEROOM,
		EAX_ENVIRONMENT_AUDITORIUM,
		EAX_ENVIRONMENT_CONCERTHALL,
		EAX_ENVIRONMENT_CAVE,
		EAX_ENVIRONMENT_ARENA,
		EAX_ENVIRONMENT_HANGAR,
		EAX_ENVIRONMENT_CARPETEDHALLWAY,
		EAX_ENVIRONMENT_HALLWAY,
		EAX_ENVIRONMENT_STONECORRIDOR,
		EAX_ENVIRONMENT_ALLEY,
		EAX_ENVIRONMENT_FOREST,
		EAX_ENVIRONMENT_CITY,
		EAX_ENVIRONMENT_MOUNTAINS,
		EAX_ENVIRONMENT_QUARRY,
		EAX_ENVIRONMENT_PLAIN,
		EAX_ENVIRONMENT_PARKINGLOT,
		EAX_ENVIRONMENT_SEWERPIPE,
		EAX_ENVIRONMENT_UNDERWATER,
		EAX_ENVIRONMENT_DRUGGED,
		EAX_ENVIRONMENT_DIZZY,
		EAX_ENVIRONMENT_PSYCHOTIC,

		EAX_ENVIRONMENT_COUNT			// total number of environments
	}

	// EAX presets, usage: BASS_SetEAXParameters(EAX_PRESET_xxx)
	float[] EAX_PRESET_GENERIC         =[EAX_ENVIRONMENT_GENERIC,0.5,1.493,0.5];
	float[] EAX_PRESET_PADDEDCELL      =[EAX_ENVIRONMENT_PADDEDCELL,0.25,0.1,0.0];
	float[] EAX_PRESET_ROOM            =[EAX_ENVIRONMENT_ROOM,0.417,0.4,0.666];
	float[] EAX_PRESET_BATHROOM        =[EAX_ENVIRONMENT_BATHROOM,0.653,1.499,0.166];
	float[] EAX_PRESET_LIVINGROOM      =[EAX_ENVIRONMENT_LIVINGROOM,0.208,0.478,0.0];
	float[] EAX_PRESET_STONEROOM       =[EAX_ENVIRONMENT_STONEROOM,0.5,2.309,0.888];
	float[] EAX_PRESET_AUDITORIUM      =[EAX_ENVIRONMENT_AUDITORIUM,0.403,4.279,0.5];
	float[] EAX_PRESET_CONCERTHALL     =[EAX_ENVIRONMENT_CONCERTHALL,0.5,3.961,0.5];
	float[] EAX_PRESET_CAVE            =[EAX_ENVIRONMENT_CAVE,0.5,2.886,1.304];
	float[] EAX_PRESET_ARENA           =[EAX_ENVIRONMENT_ARENA,0.361,7.284,0.332];
	float[] EAX_PRESET_HANGAR          =[EAX_ENVIRONMENT_HANGAR,0.5,10.0,0.3];
	float[] EAX_PRESET_CARPETEDHALLWAY =[EAX_ENVIRONMENT_CARPETEDHALLWAY,0.153,0.259,2.0];
	float[] EAX_PRESET_HALLWAY         =[EAX_ENVIRONMENT_HALLWAY,0.361,1.493,0.0];
	float[] EAX_PRESET_STONECORRIDOR   =[EAX_ENVIRONMENT_STONECORRIDOR,0.444,2.697,0.638];
	float[] EAX_PRESET_ALLEY           =[EAX_ENVIRONMENT_ALLEY,0.25,1.752,0.776];
	float[] EAX_PRESET_FOREST          =[EAX_ENVIRONMENT_FOREST,0.111,3.145,0.472];
	float[] EAX_PRESET_CITY            =[EAX_ENVIRONMENT_CITY,0.111,2.767,0.224];
	float[] EAX_PRESET_MOUNTAINS       =[EAX_ENVIRONMENT_MOUNTAINS,0.194,7.841,0.472];
	float[] EAX_PRESET_QUARRY          =[EAX_ENVIRONMENT_QUARRY,1.0,1.499,0.5];
	float[] EAX_PRESET_PLAIN           =[EAX_ENVIRONMENT_PLAIN,0.097,2.767,0.224];
	float[] EAX_PRESET_PARKINGLOT      =[EAX_ENVIRONMENT_PARKINGLOT,0.208,1.652,1.5];
	float[] EAX_PRESET_SEWERPIPE       =[EAX_ENVIRONMENT_SEWERPIPE,0.652,2.886,0.25];
	float[] EAX_PRESET_UNDERWATER      =[EAX_ENVIRONMENT_UNDERWATER,1.0,1.499,0.0];
	float[] EAX_PRESET_DRUGGED         =[EAX_ENVIRONMENT_DRUGGED,0.875,8.392,1.388];
	float[] EAX_PRESET_DIZZY           =[EAX_ENVIRONMENT_DIZZY,0.139,17.234,0.666];
	float[] EAX_PRESET_PSYCHOTIC       =[EAX_ENVIRONMENT_PSYCHOTIC,0.486,7.563,0.806];

	const int BASS_STREAMPROC_END = 0x80000000;// end of user stream flag

	// BASS_StreamCreateFileUser file systems
	const int STREAMFILE_NOBUFFER = 0;
	const int STREAMFILE_BUFFER = 1;
	const int STREAMFILE_BUFFERPUSH = 2;

	struct BASS_FILEPROCS {
		FILECLOSEPROC close;
		FILELENPROC length;
		FILEREADPROC read;
		FILESEEKPROC seek;
	}

	// BASS_StreamPutFileData options
	const int BASS_FILEDATA_END = 0;

	// BASS_StreamGetFilePosition modes
	const int BASS_FILEPOS_CURRENT = 0;
	const int BASS_FILEPOS_DECODE = BASS_FILEPOS_CURRENT;
	const int BASS_FILEPOS_DOWNLOAD = 1;
	const int BASS_FILEPOS_END = 2;
	const int BASS_FILEPOS_START = 3;
	const int BASS_FILEPOS_CONNECTED = 4;
	const int BASS_FILEPOS_BUFFER = 5;
	const int BASS_FILEPOS_SOCKET = 6;
	
	// BASS_ChannelSetSync types
	const int BASS_SYNC_POS = 0;
	const int BASS_SYNC_END = 2;
	const int BASS_SYNC_META = 4;
	const int BASS_SYNC_SLIDE = 5;
	const int BASS_SYNC_STALL = 6;
	const int BASS_SYNC_DOWNLOAD = 7;
	const int BASS_SYNC_FREE = 8;
	const int BASS_SYNC_SETPOS = 11;
	const int BASS_SYNC_MUSICPOS = 10;
	const int BASS_SYNC_MUSICINST = 1;
	const int BASS_SYNC_MUSICFX = 3;
	const int BASS_SYNC_OGG_CHANGE = 12;
	const int BASS_SYNC_MIXTIME = 0x40000000;// FLAG: sync at mixtime, else at playtime
	const int BASS_SYNC_ONETIME = 0x80000000;// FLAG: sync only once, else continuously
	
	// BASS_ChannelIsActive return values
	const int BASS_ACTIVE_STOPPED = 0;
	const int BASS_ACTIVE_PLAYING = 1;
	const int BASS_ACTIVE_STALLED = 2;
	const int BASS_ACTIVE_PAUSED = 3;

	// Channel attributes
	const int BASS_ATTRIB_FREQ = 1;
	const int BASS_ATTRIB_VOL = 2;
	const int BASS_ATTRIB_PAN = 3;
	const int BASS_ATTRIB_EAXMIX = 4;
	const int BASS_ATTRIB_NOBUFFER = 5;
	const int BASS_ATTRIB_CPU = 7;
	const int BASS_ATTRIB_SRC = 8;
	const int BASS_ATTRIB_MUSIC_AMPLIFY = 0x100;
	const int BASS_ATTRIB_MUSIC_PANSEP = 0x101;
	const int BASS_ATTRIB_MUSIC_PSCALER = 0x102;
	const int BASS_ATTRIB_MUSIC_BPM = 0x103;
	const int BASS_ATTRIB_MUSIC_SPEED = 0x104;
	const int BASS_ATTRIB_MUSIC_VOL_GLOBAL = 0x105;
	const int BASS_ATTRIB_MUSIC_VOL_CHAN = 0x200;// + channel //
	const int BASS_ATTRIB_MUSIC_VOL_INST = 0x300;// + instrument //

	// BASS_ChannelGetData flags
	const int BASS_DATA_AVAILABLE = 0;// query how much data is buffered
	const int BASS_DATA_FLOAT = 0x40000000;// flag: return floating-point sample data
	const int BASS_DATA_FFT256 = 0x80000000;//-2147483648// 256 sample FFT
	const int BASS_DATA_FFT512 = 0x80000001;//-2147483647// 512 FFT
	const int BASS_DATA_FFT1024 = 0x80000002;//-2147483646// 1024 FFT
	const int BASS_DATA_FFT2048 = 0x80000003;//-2147483645// 2048 FFT
	const int BASS_DATA_FFT4096 = 0x80000004;//-2147483644// 4096 FFT
	const int BASS_DATA_FFT8192 = 0x80000005;//-2147483643// 8192 FFT
	const int BASS_DATA_FFT16384 = 0x80000006;// 16384 FFT
	const int BASS_DATA_FFT_INDIVIDUAL = 0x10;// FFT flag: FFT for each channel, else all combined
	const int BASS_DATA_FFT_NOWINDOW = 0x20;// FFT flag: no Hanning window
	const int BASS_DATA_FFT_REMOVEDC = 0x40;// FFT flag: pre-remove DC bias
	const int BASS_DATA_FFT_COMPLEX = 0x80;// FFT flag: return complex data

	// BASS_ChannelGetTags types : what's returned
	const int BASS_TAG_ID3 = 0;// ID3v1 tags : TAG_ID3 structure
	const int BASS_TAG_ID3V2 = 1;// ID3v2 tags : variable length block
	const int BASS_TAG_OGG = 2;// OGG comments : series of null-terminated UTF-8 strings
	const int BASS_TAG_HTTP = 3;// HTTP headers : series of null-terminated ANSI strings
	const int BASS_TAG_ICY = 4;// ICY headers : series of null-terminated ANSI strings
	const int BASS_TAG_META = 5;// ICY metadata : ANSI string
	const int BASS_TAG_APE = 6;// APE tags : series of null-terminated UTF-8 strings
	const int BASS_TAG_MP4 = 7;// MP4/iTunes metadata : series of null-terminated UTF-8 strings
	const int BASS_TAG_VENDOR = 9;// OGG encoder : UTF-8 string
	const int BASS_TAG_LYRICS3 = 10;// Lyric3v2 tag : ASCII string
	const int BASS_TAG_CA_CODEC = 11;// CoreAudio codec info : TAG_CA_CODEC structure
	const int BASS_TAG_MF = 13;// Media Foundation tags : series of null-terminated UTF-8 strings
	const int BASS_TAG_WAVEFORMAT = 14;// WAVE format : WAVEFORMATEEX structure
	const int BASS_TAG_RIFF_INFO = 0x100;// RIFF "INFO" tags : series of null-terminated ANSI strings
	const int BASS_TAG_RIFF_BEXT = 0x101;// RIFF/BWF "bext" tags : TAG_BEXT structure
	const int BASS_TAG_RIFF_CART = 0x102;// RIFF/BWF "cart" tags : TAG_CART structure
	const int BASS_TAG_RIFF_DISP = 0x103;// RIFF "DISP" text tag : ANSI string
	const int BASS_TAG_APE_BINARY = 0x1000;// + index //, binary APE tag : TAG_APE_BINARY structure
	const int BASS_TAG_MUSIC_NAME = 0x10000;// MOD music name : ANSI string
	const int BASS_TAG_MUSIC_MESSAGE = 0x10001;// MOD message : ANSI string
	const int BASS_TAG_MUSIC_ORDERS = 0x10002;// MOD order list : BYTE array of pattern numbers
	const int BASS_TAG_MUSIC_INST = 0x10100;// + instrument //, MOD instrument name : ANSI string
	const int BASS_TAG_MUSIC_SAMPLE = 0x10300;// + sample //, MOD sample name : ANSI string
	
	// ID3v1 tag structure
	struct TAG_ID3 {
		char[3] id;
		char[30] title;
		char[30] artist;
		char[30] album;
		char[4] year;
		char[30] comment;
		BYTE genre;
	}

	// Binary APE tag structure
	struct TAG_APE_BINARY {
		const char *key;
		const void *data;
		DWORD length;
	}
	
	// BWF "bext" tag structure
	struct TAG_BEXT {
		align (1):
			char[256] Description;			// description
			char[32] Originator;			// name of the originator
			char[32] OriginatorReference;	// reference of the originator
			char[10] OriginationDate;		// date of creation (yyyy-mm-dd)
			char[8] OriginationTime;		// time of creation (hh-mm-ss)
			QWORD TimeReference;			// first sample count since midnight (little-endian)
			WORD Version;					// BWF version (little-endian)
			BYTE[64] UMID;					// SMPTE UMID
			BYTE[190] Reserved;
			char[] CodingHistory;			// history
	}
	
	// BWF "cart" tag structures
	struct TAG_CART_TIMER {
		DWORD dwUsage;					// FOURCC timer usage ID
		DWORD dwValue;					// timer value in samples from head
	}

	struct TAG_CART {
		char[4] Version;				// version of the data structure
		char[64] Title;					// title of cart audio sequence
		char[64] Artist;				// artist or creator name
		char[64] CutID;					// cut number identification
		char[64] ClientID;				// client identification
		char[64] Category;				// category ID, PSA, NEWS, etc
		char[64] Classification;		// classification or auxiliary key
		char[64] OutCue;				// out cue text
		char[10] StartDate;				// yyyy-mm-dd
		char[8] StartTime;				// hh:mm:ss
		char[10] EndDate;				// yyyy-mm-dd
		char[8] EndTime;				// hh:mm:ss
		char[64] ProducerAppID;			// name of vendor or application
		char[64] ProducerAppVersion;	// version of producer application
		char[64] UserDef;				// user defined text
		DWORD dwLevelReference;			// sample value for 0 dB reference
		TAG_CART_TIMER[8] PostTimer;	// 8 time markers after head
		char[276] Reserved;
		char[1024] URL;					// uniform resource locator
		char[] TagText;					// free form text for scripts or tags
	}

	// CoreAudio codec info structure
	struct TAG_CA_CODEC {
		DWORD ftype;					// file format
		DWORD atype;					// audio format
		const char *name;				// description
	}

	struct tWAVEFORMATEX
	{
		align(1):
			WORD wFormatTag;
			WORD nChannels;
			DWORD nSamplesPerSec;
			DWORD nAvgBytesPerSec;
			WORD nBlockAlign;
			WORD wBitsPerSample;
			WORD cbSize;
	} 
	alias tWAVEFORMATEX WAVEFORMATEX;
	alias tWAVEFORMATEX* PWAVEFORMATEX, LPWAVEFORMATEX, LPCWAVEFORMATEX;
	
	// BASS_ChannelGetLength/GetPosition/SetPosition modes
	const int BASS_POS_BYTE = 0;// byte position
	const int BASS_POS_MUSIC_ORDER = 1;// order.row position, MAKELONG(order,row)
	const int BASS_POS_OGG = 3;// OGG bitstream number
	const int BASS_POS_DECODE = 0x10000000;// flag: get the decoding (not playing) position
	const int BASS_POS_DECODETO = 0x20000000;// flag: decode to the position instead of seeking

	// BASS_RecordSetInput flags
	const int BASS_INPUT_OFF = 0x10000;
	const int BASS_INPUT_ON = 0x20000;
	const int BASS_INPUT_TYPE_MASK = 0xff000000;//-16777216
	const int BASS_INPUT_TYPE_UNDEF = 0x00000000;
	const int BASS_INPUT_TYPE_DIGITAL = 0x01000000;
	const int BASS_INPUT_TYPE_LINE = 0x02000000;
	const int BASS_INPUT_TYPE_MIC = 0x03000000;
	const int BASS_INPUT_TYPE_SYNTH = 0x04000000;
	const int BASS_INPUT_TYPE_CD = 0x05000000;
	const int BASS_INPUT_TYPE_PHONE = 0x06000000;
	const int BASS_INPUT_TYPE_SPEAKER = 0x07000000;
	const int BASS_INPUT_TYPE_WAVE = 0x08000000;
	const int BASS_INPUT_TYPE_AUX = 0x09000000;
	const int BASS_INPUT_TYPE_ANALOG = 0x0a000000;
	
	// DX8 effect types, use with BASS_ChannelSetFX
	enum
	{
		BASS_FX_DX8_CHORUS,
		BASS_FX_DX8_COMPRESSOR,
		BASS_FX_DX8_DISTORTION,
		BASS_FX_DX8_ECHO,
		BASS_FX_DX8_FLANGER,
		BASS_FX_DX8_GARGLE,
		BASS_FX_DX8_I3DL2REVERB,
		BASS_FX_DX8_PARAMEQ,
		BASS_FX_DX8_REVERB
	}

	struct BASS_DX8_CHORUS {
		float       fWetDryMix;
		float       fDepth;
		float       fFeedback;
		float       fFrequency;
		DWORD       lWaveform;	// 0=triangle, 1=sine
		float       fDelay;
		DWORD       lPhase;		// BASS_DX8_PHASE_xxx
	}

	struct BASS_DX8_COMPRESSOR {
		float   fGain;
		float   fAttack;
		float   fRelease;
		float   fThreshold;
		float   fRatio;
		float   fPredelay;
	}

	struct BASS_DX8_DISTORTION {
		float   fGain;
		float   fEdge;
		float   fPostEQCenterFrequency;
		float   fPostEQBandwidth;
		float   fPreLowpassCutoff;
	}

	struct BASS_DX8_ECHO {
		float   fWetDryMix;
		float   fFeedback;
		float   fLeftDelay;
		float   fRightDelay;
		BOOL    lPanDelay;
	}

	struct BASS_DX8_FLANGER {
		float       fWetDryMix;
		float       fDepth;
		float       fFeedback;
		float       fFrequency;
		DWORD       lWaveform;	// 0=triangle, 1=sine
		float       fDelay;
		DWORD       lPhase;		// BASS_DX8_PHASE_xxx
	}

	struct BASS_DX8_GARGLE {
		DWORD       dwRateHz;               // Rate of modulation in hz
		DWORD       dwWaveShape;            // 0=triangle, 1=square
	}

	struct BASS_DX8_I3DL2REVERB {
		int     lRoom;                  // [-10000, 0]      default: -1000 mB
		int     lRoomHF;                // [-10000, 0]      default: 0 mB
		float   flRoomRolloffFactor;    // [0.0, 10.0]      default: 0.0
		float   flDecayTime;            // [0.1, 20.0]      default: 1.49s
		float   flDecayHFRatio;         // [0.1, 2.0]       default: 0.83
		int     lReflections;           // [-10000, 1000]   default: -2602 mB
		float   flReflectionsDelay;     // [0.0, 0.3]       default: 0.007 s
		int     lReverb;                // [-10000, 2000]   default: 200 mB
		float   flReverbDelay;          // [0.0, 0.1]       default: 0.011 s
		float   flDiffusion;            // [0.0, 100.0]     default: 100.0 %
		float   flDensity;              // [0.0, 100.0]     default: 100.0 %
		float   flHFReference;          // [20.0, 20000.0]  default: 5000.0 Hz
	}

	struct BASS_DX8_PARAMEQ {
		float   fCenter;
		float   fBandwidth;
		float   fGain;
	}

	struct BASS_DX8_REVERB {
		float   fInGain;                // [-96.0,0.0]            default: 0.0 dB
		float   fReverbMix;             // [-96.0,0.0]            default: 0.0 db
		float   fReverbTime;            // [0.001,3000.0]         default: 1000.0 ms
		float   fHighFreqRTRatio;       // [0.001,0.999]          default: 0.001
	}
	
	const int BASS_DX8_PHASE_NEG_180 = 0;
	const int BASS_DX8_PHASE_NEG_90 = 1;
	const int BASS_DX8_PHASE_ZERO = 2;
	const int BASS_DX8_PHASE_90 = 3;
	const int BASS_DX8_PHASE_180 = 4;
	
	const int BASS_IOSNOTIFY_INTERRUPT = 1;// interruption started
	const int BASS_IOSNOTIFY_INTERRUPT_END = 2;// interruption ended

	alias DWORD function(HSTREAM handle, void *buffer, DWORD length, void *user) STREAMPROC;
	
	// User file stream callback functions
	alias void function(void *user) FILECLOSEPROC;
	alias QWORD function(void *user) FILELENPROC;
	alias DWORD function(void *buffer, DWORD length, void *user) FILEREADPROC;
	alias BOOL function(QWORD offset, void *user) FILESEEKPROC;
	
	alias void function(void *buffer, DWORD length, void *user) DOWNLOADPROC;
	
	alias void function(HSYNC handle, DWORD channel, DWORD data, void *user) SYNCPROC;
	
	alias void function(HDSP handle, DWORD channel, void *buffer, DWORD length, void *user) DSPPROC;
	
	alias BOOL function(HRECORD handle, void *buffer, DWORD length, void *user) RECORDPROC;
	
	alias void function(DWORD status) IOSNOTIFYPROC;
	
	private {		
		alias BOOL function(DWORD option, DWORD value) BASS_SETCONFIG;
		alias DWORD function(DWORD option) BASS_GETCONFIG;
		alias BOOL function(DWORD option, void *value) BASS_SETCONFIGPTR;
		alias void* function(DWORD option) BASS_GETCONFIGPTR;
		alias DWORD function() BASS_GETVERSION;
		alias int function() BASS_ERRORGETCODE;
		alias BOOL function(DWORD device, BASS_DEVICEINFO *info) BASS_GETDEVICEINFO;
		alias BOOL function(int device, DWORD freq, DWORD flags, void *win, void *dsguid) BASS_INIT;
		alias BOOL function(DWORD device) BASS_SETDEVICE;
		alias DWORD function() BASS_GETDEVICE;
		alias BOOL function() BASS_FREE;
		version (Windows) {
			alias void *function(DWORD object) BASS_GETDSOUNDOBJECT;
		}
		alias BOOL function(BASS_INFO *info) BASS_GETINFO;
		alias BOOL function(DWORD length) BASS_UPDATE;
		alias float function() BASS_GETCPU;
		alias BOOL function() BASS_START;
		alias BOOL function() BASS_STOP;
		alias BOOL function() BASS_PAUSE;
		alias BOOL function(float volume) BASS_SETVOLUME;
		alias float function() BASS_GETVOLUME;

		alias HPLUGIN function(char *file, DWORD flags) BASS_PLUGINLOAD;
		alias BOOL function(HPLUGIN handle) BASS_PLUGINFREE;
		alias BASS_PLUGININFO *function(HPLUGIN handle) BASS_PLUGINGETINFO;

		alias BOOL function(float distf, float rollf, float doppf) BASS_SET3DFACTORS;
		alias BOOL function(float *distf, float *rollf, float *doppf) BASS_GET3DFACTORS;
		alias BOOL function(BASS_3DVECTOR *pos, BASS_3DVECTOR *vel, BASS_3DVECTOR *front, BASS_3DVECTOR *top) BASS_SET3DPOSITION;
		alias BOOL function(BASS_3DVECTOR *pos, BASS_3DVECTOR *vel, BASS_3DVECTOR *front, BASS_3DVECTOR *top) BASS_GET3DPOSITION;
		alias void function() BASS_APPLY3D;
		version (Windows) {
			alias BOOL function(int env, float vol, float decay, float damp) BASS_SETEAXPARAMETERS;
			alias BOOL function(DWORD *env, float *vol, float *decay, float *damp) BASS_GETEAXPARAMETERS;
		}

		alias HMUSIC function(BOOL mem, void *file, QWORD offset, DWORD length, DWORD flags, DWORD freq) BASS_MUSICLOAD;
		alias BOOL function(HMUSIC handle) BASS_MUSICFREE;

		alias HSAMPLE function(BOOL mem, void *file, QWORD offset, DWORD length, DWORD max, DWORD flags) BASS_SAMPLELOAD;
		alias HSAMPLE function(DWORD length, DWORD freq, DWORD chans, DWORD max, DWORD flags) BASS_SAMPLECREATE;
		alias BOOL function(HSAMPLE handle) BASS_SAMPLEFREE;
		alias BOOL function(HSAMPLE handle, void *buffer) BASS_SAMPLESETDATA;
		alias BOOL function(HSAMPLE handle, void *buffer) BASS_SAMPLEGETDATA;
		alias BOOL function(HSAMPLE handle, BASS_SAMPLE *info) BASS_SAMPLEGETINFO;
		alias BOOL function(HSAMPLE handle, BASS_SAMPLE *info) BASS_SAMPLESETINFO;
		alias HCHANNEL function(HSAMPLE handle, BOOL onlynew) BASS_SAMPLEGETCHANNEL;
		alias DWORD function(HSAMPLE handle, HCHANNEL *channels) BASS_SAMPLEGETCHANNELS;
		alias BOOL function(HSAMPLE handle) BASS_SAMPLESTOP;

		alias HSTREAM function(DWORD freq, DWORD chans, DWORD flags, STREAMPROC *proc, void *user) BASS_STREAMCREATE;
		alias HSTREAM function(BOOL mem, void *file, QWORD offset, QWORD length, DWORD flags) BASS_STREAMCREATEFILE;
		alias HSTREAM function(char *url, DWORD offset, DWORD flags, DOWNLOADPROC *proc, void *user) BASS_STREAMCREATEURL;
		alias HSTREAM function(DWORD system, DWORD flags, BASS_FILEPROCS *proc, void *user) BASS_STREAMCREATEFILEUSER;
		alias BOOL function(HSTREAM handle) BASS_STREAMFREE;
		alias QWORD function(HSTREAM handle, DWORD mode) BASS_STREAMGETFILEPOSITION;
		alias DWORD function(HSTREAM handle, void *buffer, DWORD length) BASS_STREAMPUTDATA;
		alias DWORD function(HSTREAM handle, void *buffer, DWORD length) BASS_STREAMPUTFILEDATA;

		alias BOOL function(DWORD device, BASS_DEVICEINFO *info) BASS_RECORDGETDEVICEINFO;
		alias BOOL function(int device) BASS_RECORDINIT;
		alias BOOL function(DWORD device) BASS_RECORDSETDEVICE;
		alias DWORD function() BASS_RECORDGETDEVICE;
		alias BOOL function() BASS_RECORDFREE;
		alias BOOL function(BASS_RECORDINFO *info) BASS_RECORDGETINFO;
		alias char *function(int input) BASS_RECORDGETINPUTNAME;
		alias BOOL function(int input, DWORD flags, float volume) BASS_RECORDSETINPUT;
		alias DWORD function(int input, float *volume) BASS_RECORDGETINPUT;
		alias HRECORD function(DWORD freq, DWORD chans, DWORD flags, RECORDPROC *proc, void *user) BASS_RECORDSTART;

		alias double function(DWORD handle, QWORD pos) BASS_CHANNELBYTES2SECONDS;
		alias QWORD function(DWORD handle, double pos) BASS_CHANNELSECONDS2BYTES;
		alias DWORD function(DWORD handle) BASS_CHANNELGETDEVICE;
		alias BOOL function(DWORD handle, DWORD device) BASS_CHANNELSETDEVICE;
		alias DWORD function(DWORD handle) BASS_CHANNELISACTIVE;
		alias BOOL function(DWORD handle, BASS_CHANNELINFO *info) BASS_CHANNELGETINFO;
		alias char *function(DWORD handle, DWORD tags) BASS_CHANNELGETTAGS;
		alias DWORD function(DWORD handle, DWORD flags, DWORD mask) BASS_CHANNELFLAGS;
		alias BOOL function(DWORD handle, DWORD length) BASS_CHANNELUPDATE;
		alias BOOL function(DWORD handle, BOOL lock) BASS_CHANNELLOCK;
		alias BOOL function(DWORD handle, BOOL restart) BASS_CHANNELPLAY;
		alias BOOL function(DWORD handle) BASS_CHANNELSTOP;
		alias BOOL function(DWORD handle) BASS_CHANNELPAUSE;
		alias BOOL function(DWORD handle, DWORD attrib, float value) BASS_CHANNELSETATTRIBUTE;
		alias BOOL function(DWORD handle, DWORD attrib, float *value) BASS_CHANNELGETATTRIBUTE;
		alias BOOL function(DWORD handle, DWORD attrib, float value, DWORD time) BASS_CHANNELSLIDEATTRIBUTE;
		alias BOOL function(DWORD handle, DWORD attrib) BASS_CHANNELISSLIDING;
		alias BOOL function(DWORD handle, int mode, float min, float max, int iangle, int oangle, float outvol) BASS_CHANNELSET3DATTRIBUTES;
		alias BOOL function(DWORD handle, DWORD *mode, float *min, float *max, DWORD *iangle, DWORD *oangle, float *outvol) BASS_CHANNELGET3DATTRIBUTES;
		alias BOOL function(DWORD handle, BASS_3DVECTOR *pos, BASS_3DVECTOR *orient, BASS_3DVECTOR *vel) BASS_CHANNELSET3DPOSITION;
		alias BOOL function(DWORD handle, BASS_3DVECTOR *pos, BASS_3DVECTOR *orient, BASS_3DVECTOR *vel) BASS_CHANNELGET3DPOSITION;
		alias QWORD function(DWORD handle, DWORD mode) BASS_CHANNELGETLENGTH;
		alias BOOL function(DWORD handle, QWORD pos, DWORD mode) BASS_CHANNELSETPOSITION;
		alias QWORD function(DWORD handle, DWORD mode) BASS_CHANNELGETPOSITION;
		alias DWORD function(DWORD handle) BASS_CHANNELGETLEVEL;
		alias DWORD function(DWORD handle, void *buffer, DWORD length) BASS_CHANNELGETDATA;
		alias HSYNC function(DWORD handle, DWORD type, QWORD param, SYNCPROC *proc, void *user) BASS_CHANNELSETSYNC;
		alias BOOL function(DWORD handle, HSYNC sync) BASS_CHANNELREMOVESYNC;
		alias HDSP function(DWORD handle, DSPPROC *proc, void *user, int priority) BASS_CHANNELSETDSP;
		alias BOOL function(DWORD handle, HDSP dsp) BASS_CHANNELREMOVEDSP;
		alias BOOL function(DWORD handle, DWORD chan) BASS_CHANNELSETLINK;
		alias BOOL function(DWORD handle, DWORD chan) BASS_CHANNELREMOVELINK;
		alias HFX function(DWORD handle, DWORD type, int priority) BASS_CHANNELSETFX;
		alias BOOL function(DWORD handle, HFX fx) BASS_CHANNELREMOVEFX;

		alias BOOL function(HFX handle, void *params) BASS_FXSETPARAMETERS;
		alias BOOL function(HFX handle, void *params) BASS_FXGETPARAMETERS;
		alias BOOL function(HFX handle) BASS_FXRESET;
	}
}

BASS_SETCONFIG BASS_SetConfig;
BASS_GETCONFIG BASS_GetConfig;
BASS_SETCONFIGPTR BASS_SetConfigPtr;
BASS_GETCONFIGPTR BASS_GetConfigPtr;
BASS_GETVERSION BASS_GetVersion;
BASS_ERRORGETCODE BASS_ErrorGetCode;
BASS_GETDEVICEINFO BASS_GetDeviceInfo;
BASS_SETDEVICE BASS_SetDevice;
BASS_GETDEVICE BASS_GetDevice;
BASS_FREE BASS_Free;
version (Windows) {
	BASS_GETDSOUNDOBJECT BASS_GetDSoundObject;
}
BASS_GETINFO BASS_GetInfo;
BASS_UPDATE BASS_Update;
BASS_GETCPU BASS_GetCPU;
BASS_START BASS_Start;
BASS_STOP BASS_Stop;
BASS_PAUSE BASS_Pause;
BASS_SETVOLUME BASS_SetVolume;
BASS_GETVOLUME BASS_GetVolume;

private BASS_PLUGINLOAD bass_pluginload;
BASS_PLUGINFREE BASS_PluginFree;
BASS_PLUGINGETINFO BASS_PluginGetInfo;

BASS_SET3DFACTORS BASS_Set3DFactors;
BASS_GET3DFACTORS BASS_Get3DFactors;
BASS_SET3DPOSITION BASS_Set3DPosition;
BASS_GET3DPOSITION BASS_Get3DPosition;
BASS_APPLY3D BASS_Apply3D;
version (Windows) {
	BASS_SETEAXPARAMETERS BASS_SetEAXParameters;
	BASS_GETEAXPARAMETERS BASS_GetEAXParameters;
}

private BASS_MUSICLOAD bass_musicload;
BASS_MUSICFREE BASS_MusicFree;

private BASS_SAMPLELOAD bass_sampleload;
BASS_SAMPLECREATE BASS_SampleCreate;
BASS_SAMPLEFREE BASS_SampleFree;
BASS_SAMPLESETDATA BASS_SampleSetData;
BASS_SAMPLEGETDATA BASS_SampleGetData;
BASS_SAMPLEGETINFO BASS_SampleGetInfo;
BASS_SAMPLESETINFO BASS_SampleSetInfo;
BASS_SAMPLEGETCHANNEL BASS_SampleGetChannel;
BASS_SAMPLEGETCHANNELS BASS_SampleGetChannels;
BASS_SAMPLESTOP BASS_SampleStop;

BASS_STREAMCREATE BASS_StreamCreate;
private BASS_STREAMCREATEFILE bass_streamcreatefile;
private BASS_STREAMCREATEURL bass_streamcreateurl;
BASS_STREAMCREATEFILEUSER BASS_StreamCreateFileUser;
BASS_STREAMFREE BASS_StreamFree;
BASS_STREAMGETFILEPOSITION BASS_StreamGetFilePosition;
BASS_STREAMPUTDATA BASS_StreamPutData;
BASS_STREAMPUTFILEDATA BASS_StreamPutFileData;

BASS_RECORDGETDEVICEINFO BASS_RecordGetDeviceInfo;
BASS_RECORDINIT BASS_RecordInit;
BASS_RECORDSETDEVICE BASS_RecordSetDevice;
BASS_RECORDGETDEVICE BASS_RecordGetDevice;
BASS_RECORDFREE BASS_RecordFree;
BASS_RECORDGETINFO BASS_RecordGetInfo;
BASS_RECORDGETINPUTNAME BASS_RecordGetInputName;
BASS_RECORDSETINPUT BASS_RecordSetInput;
BASS_RECORDGETINPUT BASS_RecordGetInput;
BASS_RECORDSTART BASS_RecordStart;

BASS_CHANNELBYTES2SECONDS BASS_ChannelBytes2Seconds;
BASS_CHANNELSECONDS2BYTES BASS_ChannelSeconds2Bytes;
BASS_CHANNELGETDEVICE BASS_ChannelGetDevice;
BASS_CHANNELSETDEVICE BASS_ChannelSetDevice;
BASS_CHANNELISACTIVE BASS_ChannelIsActive;
BASS_CHANNELGETINFO BASS_ChannelGetInfo;
BASS_CHANNELGETTAGS BASS_ChannelGetTags;
BASS_CHANNELFLAGS BASS_ChannelFlags;
BASS_CHANNELUPDATE BASS_ChannelUpdate;
BASS_CHANNELLOCK BASS_ChannelLock;
BASS_CHANNELPLAY BASS_ChannelPlay;
BASS_CHANNELSTOP BASS_ChannelStop;
BASS_CHANNELPAUSE BASS_ChannelPause;
BASS_CHANNELSETATTRIBUTE BASS_ChannelSetAttribute;
BASS_CHANNELGETATTRIBUTE BASS_ChannelGetAttribute;
BASS_CHANNELSLIDEATTRIBUTE BASS_ChannelSlideAttribute;
BASS_CHANNELISSLIDING BASS_ChannelIsSliding;
BASS_CHANNELSET3DATTRIBUTES BASS_ChannelSet3DAttributes;
BASS_CHANNELGET3DATTRIBUTES BASS_ChannelGet3DAttributes;
BASS_CHANNELSET3DPOSITION BASS_ChannelSet3DPosition;
BASS_CHANNELGET3DPOSITION BASS_ChannelGet3DPosition;
BASS_CHANNELGETLENGTH BASS_ChannelGetLength;
BASS_CHANNELSETPOSITION BASS_ChannelSetPosition;
BASS_CHANNELGETPOSITION BASS_ChannelGetPosition;
BASS_CHANNELGETLEVEL BASS_ChannelGetLevel;
BASS_CHANNELGETDATA BASS_ChannelGetData;
BASS_CHANNELSETSYNC BASS_ChannelSetSync;
BASS_CHANNELREMOVESYNC BASS_ChannelRemoveSync;
BASS_CHANNELSETDSP BASS_ChannelSetDSP;
BASS_CHANNELREMOVEDSP BASS_ChannelRemoveDSP;
BASS_CHANNELSETLINK BASS_ChannelSetLink;
BASS_CHANNELREMOVELINK BASS_ChannelRemoveLink;
BASS_CHANNELSETFX BASS_ChannelSetFX;
BASS_CHANNELREMOVEFX BASS_ChannelRemoveFX;

BASS_FXSETPARAMETERS BASS_FXSetParameters;
BASS_FXGETPARAMETERS BASS_FXGetParameters;
BASS_FXRESET BASS_FXReset;

private BASS_INIT bass_init;

uint BASS_Init(int device, uint freq, uint flags, void* win, void* dsguid) {
	if (initialized) {
		return bass_init(device, freq, flags, win, dsguid);
	}

	void* lib = loadLibrary(libPath.toStringz);
	
	BASS_SetConfig = cast(BASS_SETCONFIG) getProc(lib, "BASS_SetConfig".toStringz);
	BASS_GetConfig = cast(BASS_GETCONFIG) getProc(lib, "BASS_GetConfig".toStringz);
	BASS_SetConfigPtr = cast(BASS_SETCONFIGPTR) getProc(lib, "BASS_SetConfigPtr".toStringz);
	BASS_GetConfigPtr = cast(BASS_GETCONFIGPTR) getProc(lib, "BASS_GetConfigPtr".toStringz);
	BASS_GetVersion = cast(BASS_GETVERSION) getProc(lib, "BASS_GetVersion".toStringz);
	BASS_ErrorGetCode = cast(BASS_ERRORGETCODE) getProc(lib, "BASS_ErrorGetCode".toStringz);
	BASS_GetDeviceInfo = cast(BASS_GETDEVICEINFO) getProc(lib, "BASS_GetDeviceInfo".toStringz);
	BASS_SetDevice = cast(BASS_SETDEVICE) getProc(lib, "BASS_SetDevice".toStringz);
	BASS_GetDevice = cast(BASS_GETDEVICE) getProc(lib, "BASS_GetDevice".toStringz);
	BASS_Free = cast(BASS_FREE) getProc(lib, "BASS_Free".toStringz);
	version (Windows) {
		BASS_GetDSoundObject = cast(BASS_GETDSOUNDOBJECT) getProc(lib, "BASS_GetDSoundObject".toStringz);
	}
	BASS_GetInfo = cast(BASS_GETINFO) getProc(lib, "BASS_GetInfo".toStringz);
	BASS_Update = cast(BASS_UPDATE) getProc(lib, "BASS_Update".toStringz);
	BASS_GetCPU = cast(BASS_GETCPU) getProc(lib, "BASS_GetCPU".toStringz);
	BASS_Start = cast(BASS_START) getProc(lib, "BASS_Start".toStringz);
	BASS_Stop = cast(BASS_STOP) getProc(lib, "BASS_Stop".toStringz);
	BASS_Pause = cast(BASS_PAUSE) getProc(lib, "BASS_Pause".toStringz);
	BASS_SetVolume = cast(BASS_SETVOLUME) getProc(lib, "BASS_SetVolume".toStringz);
	BASS_GetVolume = cast(BASS_GETVOLUME) getProc(lib, "BASS_GetVolume".toStringz);

	bass_pluginload = cast(BASS_PLUGINLOAD) getProc(lib, "BASS_PluginLoad".toStringz);
	BASS_PluginFree = cast(BASS_PLUGINFREE) getProc(lib, "BASS_PluginFree".toStringz);
	BASS_PluginGetInfo = cast(BASS_PLUGINGETINFO) getProc(lib, "BASS_PluginGetInfo".toStringz);

	BASS_Set3DFactors = cast(BASS_SET3DFACTORS) getProc(lib, "BASS_Set3DFactors".toStringz);
	BASS_Get3DFactors = cast(BASS_GET3DFACTORS) getProc(lib, "BASS_Get3DFactors".toStringz);
	BASS_Set3DPosition = cast(BASS_SET3DPOSITION) getProc(lib, "BASS_Set3DPosition".toStringz);
	BASS_Get3DPosition = cast(BASS_GET3DPOSITION) getProc(lib, "BASS_Get3DPosition".toStringz);
	BASS_Apply3D = cast(BASS_APPLY3D) getProc(lib, "BASS_Apply3D".toStringz);
	version (Windows) {
		BASS_SetEAXParameters = cast(BASS_SETEAXPARAMETERS) getProc(lib, "BASS_SetEAXParameters".toStringz);
		BASS_GetEAXParameters = cast(BASS_GETEAXPARAMETERS) getProc(lib, "BASS_GetEAXParameters".toStringz);
	}

	bass_musicload = cast(BASS_MUSICLOAD) getProc(lib, "BASS_MusicLoad".toStringz);
	BASS_MusicFree = cast(BASS_MUSICFREE) getProc(lib, "BASS_MusicFree".toStringz);

	bass_sampleload = cast(BASS_SAMPLELOAD) getProc(lib, "BASS_SampleLoad".toStringz);
	BASS_SampleCreate = cast(BASS_SAMPLECREATE) getProc(lib, "BASS_SampleCreate".toStringz);
	BASS_SampleFree = cast(BASS_SAMPLEFREE) getProc(lib, "BASS_SampleFree".toStringz);
	BASS_SampleSetData = cast(BASS_SAMPLESETDATA) getProc(lib, "BASS_SampleSetData".toStringz);
	BASS_SampleGetData = cast(BASS_SAMPLEGETDATA) getProc(lib, "BASS_SampleGetData".toStringz);
	BASS_SampleGetInfo = cast(BASS_SAMPLEGETINFO) getProc(lib, "BASS_SampleGetInfo".toStringz);
	BASS_SampleSetInfo = cast(BASS_SAMPLESETINFO) getProc(lib, "BASS_SampleSetInfo".toStringz);
	BASS_SampleGetChannel = cast(BASS_SAMPLEGETCHANNEL) getProc(lib, "BASS_SampleGetChannel".toStringz);
	BASS_SampleGetChannels = cast(BASS_SAMPLEGETCHANNELS) getProc(lib, "BASS_SampleGetChannels".toStringz);
	BASS_SampleStop = cast(BASS_SAMPLESTOP) getProc(lib, "BASS_SampleStop".toStringz);

	BASS_StreamCreate = cast(BASS_STREAMCREATE) getProc(lib, "BASS_StreamCreate".toStringz);
	bass_streamcreatefile = cast(BASS_STREAMCREATEFILE) getProc(lib, "BASS_StreamCreateFile".toStringz);
	bass_streamcreateurl = cast(BASS_STREAMCREATEURL) getProc(lib, "BASS_StreamCreateURL".toStringz);
	BASS_StreamCreateFileUser = cast(BASS_STREAMCREATEFILEUSER) getProc(lib, "BASS_StreamCreateFileUser".toStringz);
	BASS_StreamFree = cast(BASS_STREAMFREE) getProc(lib, "BASS_StreamFree".toStringz);
	BASS_StreamGetFilePosition = cast(BASS_STREAMGETFILEPOSITION) getProc(lib, "BASS_StreamGetFilePosition".toStringz);
	BASS_StreamPutData = cast(BASS_STREAMPUTDATA) getProc(lib, "BASS_StreamPutData".toStringz);
	BASS_StreamPutFileData = cast(BASS_STREAMPUTFILEDATA) getProc(lib, "BASS_StreamPutFileData".toStringz);

	BASS_RecordGetDeviceInfo = cast(BASS_RECORDGETDEVICEINFO) getProc(lib, "BASS_RecordGetDeviceInfo".toStringz);
	BASS_RecordInit = cast(BASS_RECORDINIT) getProc(lib, "BASS_RecordInit".toStringz);
	BASS_RecordSetDevice = cast(BASS_RECORDSETDEVICE) getProc(lib, "BASS_RecordSetDevice".toStringz);
	BASS_RecordGetDevice = cast(BASS_RECORDGETDEVICE) getProc(lib, "BASS_RecordGetDevice".toStringz);
	BASS_RecordFree = cast(BASS_RECORDFREE) getProc(lib, "BASS_RecordFree".toStringz);
	BASS_RecordGetInfo = cast(BASS_RECORDGETINFO) getProc(lib, "BASS_RecordGetInfo".toStringz);
	BASS_RecordGetInputName = cast(BASS_RECORDGETINPUTNAME) getProc(lib, "BASS_RecordGetInputName".toStringz);
	BASS_RecordSetInput = cast(BASS_RECORDSETINPUT) getProc(lib, "BASS_RecordSetInput".toStringz);
	BASS_RecordGetInput = cast(BASS_RECORDGETINPUT) getProc(lib, "BASS_RecordGetInput".toStringz);
	BASS_RecordStart = cast(BASS_RECORDSTART) getProc(lib, "BASS_RecordStart".toStringz);

	BASS_ChannelBytes2Seconds = cast(BASS_CHANNELBYTES2SECONDS) getProc(lib, "BASS_ChannelBytes2Seconds".toStringz);
	BASS_ChannelSeconds2Bytes = cast(BASS_CHANNELSECONDS2BYTES) getProc(lib, "BASS_ChannelSeconds2Bytes".toStringz);
	BASS_ChannelGetDevice = cast(BASS_CHANNELGETDEVICE) getProc(lib, "BASS_ChannelGetDevice".toStringz);
	BASS_ChannelSetDevice = cast(BASS_CHANNELSETDEVICE) getProc(lib, "BASS_ChannelSetDevice".toStringz);
	BASS_ChannelIsActive = cast(BASS_CHANNELISACTIVE) getProc(lib, "BASS_ChannelIsActive".toStringz);
	BASS_ChannelGetInfo = cast(BASS_CHANNELGETINFO) getProc(lib, "BASS_ChannelGetInfo".toStringz);
	BASS_ChannelGetTags = cast(BASS_CHANNELGETTAGS) getProc(lib, "BASS_ChannelGetTags".toStringz);
	BASS_ChannelFlags = cast(BASS_CHANNELFLAGS) getProc(lib, "BASS_ChannelFlags".toStringz);
	BASS_ChannelUpdate = cast(BASS_CHANNELUPDATE) getProc(lib, "BASS_ChannelUpdate".toStringz);
	BASS_ChannelLock = cast(BASS_CHANNELLOCK) getProc(lib, "BASS_ChannelLock".toStringz);
	BASS_ChannelPlay = cast(BASS_CHANNELPLAY) getProc(lib, "BASS_ChannelPlay".toStringz);
	BASS_ChannelStop = cast(BASS_CHANNELSTOP) getProc(lib, "BASS_ChannelStop".toStringz);
	BASS_ChannelPause = cast(BASS_CHANNELPAUSE) getProc(lib, "BASS_ChannelPause".toStringz);
	BASS_ChannelSetAttribute = cast(BASS_CHANNELSETATTRIBUTE) getProc(lib, "BASS_ChannelSetAttribute".toStringz);
	BASS_ChannelGetAttribute = cast(BASS_CHANNELGETATTRIBUTE) getProc(lib, "BASS_ChannelGetAttribute".toStringz);
	BASS_ChannelSlideAttribute = cast(BASS_CHANNELSLIDEATTRIBUTE) getProc(lib, "BASS_ChannelSlideAttribute".toStringz);
	BASS_ChannelIsSliding = cast(BASS_CHANNELISSLIDING) getProc(lib, "BASS_ChannelIsSliding".toStringz);
	BASS_ChannelSet3DAttributes = cast(BASS_CHANNELSET3DATTRIBUTES) getProc(lib, "BASS_ChannelSet3DAttributes".toStringz);
	BASS_ChannelGet3DAttributes = cast(BASS_CHANNELGET3DATTRIBUTES) getProc(lib, "BASS_ChannelGet3DAttributes".toStringz);
	BASS_ChannelSet3DPosition = cast(BASS_CHANNELSET3DPOSITION) getProc(lib, "BASS_ChannelSet3DPosition".toStringz);
	BASS_ChannelGet3DPosition = cast(BASS_CHANNELGET3DPOSITION) getProc(lib, "BASS_ChannelGet3DPosition".toStringz);
	BASS_ChannelGetLength = cast(BASS_CHANNELGETLENGTH) getProc(lib, "BASS_ChannelGetLength".toStringz);
	BASS_ChannelSetPosition = cast(BASS_CHANNELSETPOSITION) getProc(lib, "BASS_ChannelSetPosition".toStringz);
	BASS_ChannelGetPosition = cast(BASS_CHANNELGETPOSITION) getProc(lib, "BASS_ChannelGetPosition".toStringz);
	BASS_ChannelGetLevel = cast(BASS_CHANNELGETLEVEL) getProc(lib, "BASS_ChannelGetLevel".toStringz);
	BASS_ChannelGetData = cast(BASS_CHANNELGETDATA) getProc(lib, "BASS_ChannelGetData".toStringz);
	BASS_ChannelSetSync = cast(BASS_CHANNELSETSYNC) getProc(lib, "BASS_ChannelSetSync".toStringz);
	BASS_ChannelRemoveSync = cast(BASS_CHANNELREMOVESYNC) getProc(lib, "BASS_ChannelRemoveSync".toStringz);
	BASS_ChannelSetDSP = cast(BASS_CHANNELSETDSP) getProc(lib, "BASS_ChannelSetDSP".toStringz);
	BASS_ChannelRemoveDSP = cast(BASS_CHANNELREMOVEDSP) getProc(lib, "BASS_ChannelRemoveDSP".toStringz);
	BASS_ChannelSetLink = cast(BASS_CHANNELSETLINK) getProc(lib, "BASS_ChannelSetLink".toStringz);
	BASS_ChannelRemoveLink = cast(BASS_CHANNELREMOVELINK) getProc(lib, "BASS_ChannelRemoveLink".toStringz);
	BASS_ChannelSetFX = cast(BASS_CHANNELSETFX) getProc(lib, "BASS_ChannelSetFX".toStringz);
	BASS_ChannelRemoveFX = cast(BASS_CHANNELREMOVEFX) getProc(lib, "BASS_ChannelRemoveFX".toStringz);

	BASS_FXSetParameters = cast(BASS_FXSETPARAMETERS) getProc(lib, "BASS_FXSetParameters".toStringz);
	BASS_FXGetParameters = cast(BASS_FXGETPARAMETERS) getProc(lib, "BASS_FXGetParameters".toStringz);
	BASS_FXReset = cast(BASS_FXRESET) getProc(lib, "BASS_FXReset".toStringz);
	
	bass_init = cast(BASS_INIT) getProc(lib, "BASS_Init".toStringz);
	initialized = true;
	return bass_init(device, freq, flags, win, dsguid);
}

HPLUGIN BASS_PluginLoad(wstring file, DWORD flags)
{
	return bass_pluginload(cast(char*)file, flags);
}

HMUSIC BASS_MusicLoad(BOOL mem, wstring file, QWORD offset, DWORD length, DWORD flags, DWORD freq)
{
	return bass_musicload(mem, cast(void*)file, offset, length, flags, freq);
}

HSAMPLE BASS_SampleLoad(BOOL mem, wstring file, QWORD offset, DWORD length, DWORD max, DWORD flags)
{
	return bass_sampleload(mem, cast(void*)file, offset, length, max, flags);
}

HSTREAM BASS_StreamCreateFile(BOOL mem, wstring file, QWORD offset, QWORD length, DWORD flags)
{
	return bass_streamcreatefile(mem, cast(void*)file, offset, length, flags);
}

HSTREAM BASS_StreamCreateURL(wstring url, DWORD offset, DWORD flags, DOWNLOADPROC *proc, void *user)
{
	return bass_streamcreateurl(cast(char*)url, offset, flags, proc, user);
}

void checkAndThrow(void* pointer, int line = __LINE__) {
	enforce(pointer !is null, format("Error number %x on line %d. ", lastError(), line));
}